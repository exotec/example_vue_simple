<?php

declare(strict_types=1);

namespace Exotec\ExampleVueSimple\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author Alexander Weebr <weber@exotec.de>
 */
class TodoControllerTest extends UnitTestCase
{
    /**
     * @var \Exotec\ExampleVueSimple\Controller\TodoController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\Exotec\ExampleVueSimple\Controller\TodoController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTodosFromRepositoryAndAssignsThemToView(): void
    {
        $allTodos = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $todoRepository = $this->getMockBuilder(\Exotec\ExampleVueSimple\Domain\Repository\TodoRepository::class)
            ->onlyMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $todoRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTodos));
        $this->subject->_set('todoRepository', $todoRepository);

        $view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('todos', $allTodos);
        $this->subject->_set('view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenTodoToTodoRepository(): void
    {
        $todo = new \Exotec\ExampleVueSimple\Domain\Model\Todo();

        $todoRepository = $this->getMockBuilder(\Exotec\ExampleVueSimple\Domain\Repository\TodoRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $todoRepository->expects(self::once())->method('add')->with($todo);
        $this->subject->_set('todoRepository', $todoRepository);

        $this->subject->createAction($todo);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenTodoToView(): void
    {
        $todo = new \Exotec\ExampleVueSimple\Domain\Model\Todo();

        $view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $this->subject->_set('view', $view);
        $view->expects(self::once())->method('assign')->with('todo', $todo);

        $this->subject->editAction($todo);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenTodoInTodoRepository(): void
    {
        $todo = new \Exotec\ExampleVueSimple\Domain\Model\Todo();

        $todoRepository = $this->getMockBuilder(\Exotec\ExampleVueSimple\Domain\Repository\TodoRepository::class)
            ->onlyMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $todoRepository->expects(self::once())->method('update')->with($todo);
        $this->subject->_set('todoRepository', $todoRepository);

        $this->subject->updateAction($todo);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenTodoFromTodoRepository(): void
    {
        $todo = new \Exotec\ExampleVueSimple\Domain\Model\Todo();

        $todoRepository = $this->getMockBuilder(\Exotec\ExampleVueSimple\Domain\Repository\TodoRepository::class)
            ->onlyMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $todoRepository->expects(self::once())->method('remove')->with($todo);
        $this->subject->_set('todoRepository', $todoRepository);

        $this->subject->deleteAction($todo);
    }
}

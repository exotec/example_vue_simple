<?php

declare(strict_types=1);

namespace Exotec\ExampleVueSimple\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Alexander Weebr <weber@exotec.de>
 */
class TodoTest extends UnitTestCase
{
    /**
     * @var \Exotec\ExampleVueSimple\Domain\Model\Todo|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Exotec\ExampleVueSimple\Domain\Model\Todo::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle(): void
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('title'));
    }

    /**
     * @test
     */
    public function getDoneReturnsInitialValueForBool(): void
    {
        self::assertFalse($this->subject->getDone());
    }

    /**
     * @test
     */
    public function setDoneForBoolSetsDone(): void
    {
        $this->subject->setDone(true);

        self::assertEquals(true, $this->subject->_get('done'));
    }
}

<?php

declare(strict_types=1);

namespace Exotec\ExampleVueSimple\Domain\Repository;


/**
 * This file is part of the "Simple Vue Example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Alexander Weebr <weber@exotec.de>, exotec
 */

/**
 * The repository for Todos
 */
class TodoRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];
}

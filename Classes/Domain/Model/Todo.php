<?php

declare(strict_types=1);

namespace Exotec\ExampleVueSimple\Domain\Model;


/**
 * This file is part of the "Simple Vue Example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Alexander Weebr <weber@exotec.de>, exotec
 */

/**
 * Todo
 */
class Todo extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = null;

    /**
     * done
     *
     * @var bool
     */
    protected $done = null;

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Returns the done
     *
     * @return bool
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Sets the done
     *
     * @param bool $done
     * @return void
     */
    public function setDone(bool $done)
    {
        $this->done = $done;
    }

    /**
     * Returns the boolean state of done
     *
     * @return bool
     */
    public function isDone()
    {
        return $this->done;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'uid' => $this->getUid(),
            'title' => $this->getTitle(),
            'done' => $this->isDone(),
        ];
    }
}

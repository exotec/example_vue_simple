<?php

declare(strict_types=1);

namespace Exotec\ExampleVueSimple\Controller;


use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * This file is part of the "Simple Vue Example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Alexander Weebr <weber@exotec.de>, exotec
 */

/**
 * TodoController
 */
class TodoController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * todoRepository
     *
     * @var \Exotec\ExampleVueSimple\Domain\Repository\TodoRepository
     */
    protected $todoRepository = null;

    /**
     * @param \Exotec\ExampleVueSimple\Domain\Repository\TodoRepository $todoRepository
     */
    public function injectTodoRepository(\Exotec\ExampleVueSimple\Domain\Repository\TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $todos = $this->todoRepository->findAll();

        $todos = array_map(function ($todo) {
            return $todo->toArray();
        }, $todos->toArray());

        $this->view->assign('json', json_encode($todos));

        return $this->htmlResponse();
    }

    /**
     * action new
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function newAction(): \Psr\Http\Message\ResponseInterface
    {
        return $this->htmlResponse();
    }

    /**
     * action create
     *
     * @param \Exotec\ExampleVueSimple\Domain\Model\Todo $newTodo
     */
    public function createAction(\Exotec\ExampleVueSimple\Domain\Model\Todo $newTodo)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->todoRepository->add($newTodo);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Exotec\ExampleVueSimple\Domain\Model\Todo $todo
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("todo")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function editAction(\Exotec\ExampleVueSimple\Domain\Model\Todo $todo): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('todo', $todo);
        return $this->htmlResponse();
    }

    /**
     * action update
     *
     * @param \Exotec\ExampleVueSimple\Domain\Model\Todo $todo
     */
    public function updateAction(\Exotec\ExampleVueSimple\Domain\Model\Todo $todo)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->todoRepository->update($todo);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Exotec\ExampleVueSimple\Domain\Model\Todo $todo
     */
    public function deleteAction(\Exotec\ExampleVueSimple\Domain\Model\Todo $todo)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->todoRepository->remove($todo);
        $this->redirect('list');
    }
}

<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'ExampleVueSimple',
        'web',
        'todo',
        '',
        [
            \Exotec\ExampleVueSimple\Controller\TodoController::class => 'list, new, create, edit, update, delete',
        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:example_vue_simple/Resources/Public/Icons/user_mod_todo.svg',
            'labels' => 'LLL:EXT:example_vue_simple/Resources/Private/Language/locallang_todo.xlf',
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_examplevuesimple_domain_model_todo', 'EXT:example_vue_simple/Resources/Private/Language/locallang_csh_tx_examplevuesimple_domain_model_todo.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_examplevuesimple_domain_model_todo');
})();

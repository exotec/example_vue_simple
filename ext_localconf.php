<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'ExampleVueSimple',
        'Todo',
        [
            \Exotec\ExampleVueSimple\Controller\TodoController::class => 'list, new, create, edit, update, delete'
        ],
        // non-cacheable actions
        [
            \Exotec\ExampleVueSimple\Controller\TodoController::class => 'create, update, delete'
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    todo {
                        iconIdentifier = example_vue_simple-plugin-todo
                        title = LLL:EXT:example_vue_simple/Resources/Private/Language/locallang_db.xlf:tx_example_vue_simple_todo.name
                        description = LLL:EXT:example_vue_simple/Resources/Private/Language/locallang_db.xlf:tx_example_vue_simple_todo.description
                        tt_content_defValues {
                            CType = list
                            list_type = examplevuesimple_todo
                        }
                    }
                }
                show = *
            }
       }'
    );
})();

<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Simple Vue Example',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Alexander Weebr',
    'author_email' => 'weber@exotec.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

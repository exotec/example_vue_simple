CREATE TABLE tx_examplevuesimple_domain_model_todo (
	title varchar(255) NOT NULL DEFAULT '',
	done smallint(1) unsigned NOT NULL DEFAULT '0'
);

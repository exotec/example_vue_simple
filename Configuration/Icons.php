<?php

return [
    'example_vue_simple-plugin-todo' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:example_vue_simple/Resources/Public/Icons/user_plugin_todo.svg'
    ],
];
